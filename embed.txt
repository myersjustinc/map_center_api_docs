.. _embed:

###############
Embeddable maps
###############

There is some support for embedding certain Map Center modules (currently
:ref:`static maps <modules-static_maps>`,
:ref:`past primary results <modules-live>` for which tabulation is complete and
the :ref:`Electoral College calculator <modules-electoral_college>`) in other
Web pages, such as those of PBS member stations.

Embeds are handled through two main files:

* A basic PHP script serves markup for a particular map to the user depending
  on its `query string`_ arguments.
* A JavaScript file provides support for creating this query string,
  calculating the height of the embed and generating the ``<iframe>`` element
  that will hold the embedded map.

.. _query string: http://en.wikipedia.org/wiki/Query_string


***********
Server side
***********

The server side of this is a PHP script (located at ``embed/embed.php``) that
just writes slightly different versions of its content depending on the query
string arguments provided. It otherwise contains most of the same elements as
the normal map pages (except for less useful ones such as the
:ref:`navigation <ui-map_page_selection>` and :ref:`tabs <ui-tabs>`) and is
designed to be responsive_ using the styles in ``embed/embed.css``.

.. _responsive: http://www.alistapart.com/articles/responsive-web-design/

The script checks that the arguments fall into the set of allowed values to
avoid weird exploits, but this means adding a new module's functionality to
this takes quite a bit of hand-holding. Ask if you need help.


***********
Client side
***********

``embed/nhmc_embed.js`` defines the ``embedNHMC`` function, which embed codes
call to create the actual ``<iframe>`` element for the map.

It calculates the height of the element (using lots of our trial and error) in
the ``calcHeight`` function, which should be tweaked as necessary. (Being able
to change this and affect all embeds is probably the main reason Map Center
embeds are structured this way, honestly.)

It also generates an ID unique to the embed's contents in case the ``<iframe>``
element needs to be styled in any way. This is currently unused, but it could
be a useful support tool later on for sites that embed this.

``embedNHMC`` appends the embedded map by default, but it can be made to just
return the ``<iframe>`` markup, which can be useful for previews such as in
``embed/index.html``. (See the source of ``embed/nhmc_embed.js`` for details.)
