.. _modules-live:

#######################################
Republican presidential primary results
#######################################

This module allows users to view the results of 2012 Republican presidential
primaries and caucuses by state.

This module lives in ``lib/map_center/modules/live.js``.

.. todo::
   Add support for viewing a
   :ref:`nationwide state map <nhmc.geo-nationwide_state>` of state winners
   such as the one currently provided by the
   :ref:`static maps module <modules-static_maps>` that would also allow users
   to click a state to see the results of that state's primary or caucuses.


.. _modules-live-operation:

*********
Operation
*********

The one :ref:`tab <ui-tabs>` available in this module allows users to select a
state in order to view the county-level results of that state's 2012 Republican
presidential primary or caucus. (Some states did not report county-level caucus
results and only reported statewide results; for those states, statewide maps
as described in :ref:`nhmc.config <nhmc.config-usps>` are used instead.)

The :ref:`sidebar <ui-sidebars>` in this module shows the statewide vote
breakdown in the race, and the :ref:`tooltips <nhmc.tooltips>` for each county
shows the countywide vote breakdown in the race.

If `config.autoRefresh`_ is ``true``, the map will request and display updated
results every `config.autoRefreshDelay`_ milliseconds. This does not require a
full page refresh.

Results in this module are provided by the Associated Press'
`AP Election Services`_.

.. _AP Election Services: http://www.ap.org/products-services/new-media


.. _modules-live-configuration_options:

*********************
Configuration options
*********************

All configuration options live in the ``config`` object toward the top of the
module source and may be overridden by defining a global object named
``nhmc_live_config``, which will be merged into ``config`` using
`jQuery.extend`_.

.. _jQuery.extend: http://api.jquery.com/jQuery.extend/

.. _config.autoRefresh:

* ``autoRefresh`` is a Boolean value stating whether the map should
  periodically request and display updated results. This is useful on election
  night to keep users from having to manually refresh the page to view updated
  results.

.. _config.autoRefreshDelay:

* ``autoRefreshDelay`` is a number stating the number of milliseconds to wait
  between requests for updated results. This is ignored if
  `config.autoRefresh`_ is ``false``.

.. _config.bigCandidates:

* ``bigCandidates`` is an integer defining the number of candidates in the
  sidebar that will have large legend entries including their photo and large
  vote percents as opposed to smaller, more compact legend entries.

.. _config.blankMap:

* ``blankMap`` is a Boolean value stating whether to force the map to have all
  counties uncolored. This was used in one broadcast segment.

.. _config.candidateColors:

* ``candidateColors`` is an object with candidate names as keys and hexadecimal
  color codes as values for colors to represent them on the map.

.. _config.candidateImages:

* ``candidateImages`` is an object with candidate names as keys and headshot
  image URLs as values.

.. _config.condenseCandidates:

* ``condenseCandidates`` is a Boolean value stating whether to restrict the
  list of candidates to those named in ``showCandidates`` and group all others
  under an "Other" entry.

.. _config.flyouts:

* ``flyouts`` is a value stating whether tooltips should be rendered as
  :ref:`flyouts <ui-flyouts>`, which is only done for broadcast. Originally it
  was intended to be a string value setting a position for the flyout (or
  ``null`` if flyouts should not be used), but this value currently is only
  checked for truthiness_. Positioning is entirely handled in CSS.
  
  .. _truthiness: http://11heavens.com/falsy-and-truthy-in-javascript

.. _config.flyoutWidth:

* ``flyoutWidth`` is a number stating the number of pixels wide to render
  tooltips if they are rendered as :ref:`flyouts <ui-flyouts>`. (This was here
  in order to support certain animations.) Positioning and formatting are
  entirely handled in CSS now, so this option should be considered deprecated.

.. _config.showCandidates:

* ``showCandidates`` is an array of candidate names to show when
  `config.condenseCandidates`_ is true.

.. _config.strokeHighlight:

* ``strokeHighlight`` is a hexadecimal color code used when
  :ref:`flyouts <ui-flyouts>` are enabled; when this is the case, this color is
  used to outline the county targeted by the tooltip.

.. _config.tooltipsEnabled:

* ``tooltipsEnabled`` is a Boolean value stating whether tooltips with
  countywide vote totals should be enabled at all.


.. _modules-live-data_structure:

**************
Data structure
**************

The results data for each state lives in a static JSONP_ file at
``http://www.pbs.org/newshour/vote2012/map/live_data/VIEW_NAME.json``, where
``VIEW_NAME`` is the state's abbreviation as found in
:ref:`nhmc.config.stateToUSPS <nhmc.config-usps>`, lowercased. The file's
callback name is the same abbreviation, but the name there is uppercased; this
was done to avoid conflicts with the lowercase abbreviation ``in`` for the
state of Indiana, which happens to be a `reserved word`_ in JavaScript.

.. _JSONP: http://en.wikipedia.org/wiki/JSONP
.. _reserved word: https://developer.mozilla.org/en/JavaScript/Reference/Reserved_Words

Each state's results object contains the following properties:

* ``areas`` is an object with area IDs as keys and objects with area results as
  values.
  
  .. note::
     For the purposes of this module, an "area ID" is generally a
     :ref:`county FIPS code <nhmc.config-fips>`; for non-county areas (such as
     states), the full name of the area is used instead.
  
  Each area's results object contains the following properties:
  
  * ``data`` is an array that contains one two-element array per candidate: The
    first element is the candidate ID listed in ``candidates``, and the second
    element is the number of votes that candidate received. The area's array is
    sorted by the number of votes received in descending order; that is, the
    candidate with the most votes in that area is listed first.
  
  * ``precincts`` is an array containing two elements:
    
    * The first is the number of precincts reporting in the area.
    * The second is the total number of precincts in the area.
    
    These may be used to calculate the percent of precincts reporting.
    
    .. warning::
       Rounding errors become significant once more than 99 percent of
       precincts are reporting; module developers should ensure they do not
       erroneously report 100 percent of precincts reporting before that is
       actually the case. (This is primarily an issue when rounding to the
       nearest integer.)

* ``breakdown`` is an array identical in format to the county ``data`` objects
  in ``areas`` but with results for the entire state.

* ``candidates`` is an object with candidate IDs as keys and candidate names as
  values. This allows the data in ``areas``, ``breakdown``, ``colors``,
  ``images`` and ``winners`` to use the more compact candidate IDs instead of
  candidate names to save space.
  
  .. note::
     All configuration options use candidates' names, not their IDs. IDs should
     not be used with any other modules; AP Election Services arbitrarily
     assigns them to candidates and may use different IDs from state to state
     for the same candidate.

* ``colors`` is an object with candidate IDs as keys and hexadecimal color
  codes as values. This is deprecated due to the introduction of the
  `config.candidateColors`_ option.

* ``images`` is an object with candidate IDs as keys and headshot image URLs
  as values. This is deprecated due to the introduction of the
  `config.candidateImages`_ option.

* ``lastUpdated`` is an array containing five elements describing the date and
  time (in the `Eastern time zone`_, including `Daylight Saving Time`_ if in
  effect) when this results file was last updated.
  
  .. _Eastern time zone: http://en.wikipedia.org/wiki/Eastern_Time_Zone
  .. _Daylight Saving Time: http://en.wikipedia.org/wiki/Daylight_saving_time_in_the_United_States

* ``precincts`` is an array containing two elements:
  
  * The first is the number of precincts reporting statewide.
  * The second is the total number of precincts statewide.
  
  These may be used to calculate the percent of precincts reporting.
  
  .. warning::
     Rounding errors become significant once more than 99 percent of precincts
     are reporting; module developers should ensure they do not erroneously
     report 100 percent of precincts reporting before that is actually the
     case. (This is primarily an issue when rounding to the nearest integer.)

* ``test`` is a Boolean value stating whether AP Election Services marked any
  of the data used to generate this results file as being test data not
  intended for publication.

* ``winners`` is an object with area IDs as keys. For each area ID, the value
  is either the candidate ID for the winner projected by AP or ``null`` if no
  such projection has yet occurred.


.. _modules-live-fragment_identifier:

*******************
Fragment identifier
*******************

This module makes use of one key-value pair in the `fragment identifier`_ via
:ref:`nhmc.ctrl.hashParams`:

.. _fragment identifier: http://en.wikipedia.org/wiki/Fragment_identifier

* ``map_view``, if provided, should be set to the view name of the state to
  display first. The fragment identifier will update as the map view is
  changed.
