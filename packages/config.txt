.. _nhmc.config:

###########
nhmc.config
###########

This package contains a variety of options related to the general appearance
and operation of the Map Center. When a Map Center page is loaded,
:ref:`nhmc.mapCenterInit` checks for the presence of a global object called
``nhmc_config``; if that object exists, its values are merged into this package
before initialization proceeds, allowing for individual pages to override these
options as necessary.


*****************
Rendering options
*****************

.. _nhmc.config.defaultDimensions:

nhmc.config.defaultDimensions
=============================

*Object*

This contains the dimensions of the original SVG_ images on which the Map
Center views are based. Several :ref:`scaling <nhmc.ctrl-scaling>` calculations
take these dimensions into account.

.. _SVG: http://en.wikipedia.org/wiki/Scalable_Vector_Graphics

.. caution::
   These dimensions should not be changed unless the source vector imagery on
   which the Map Center views are based also have changed. If you're attempting
   to change the size of the map, do so by setting the size of the ``#map``
   element in CSS.

.. _nhmc.config.countySteps:

nhmc.config.countySteps
=======================

*Object*

The Map Center renders maps containing counties gradually to avoid locking up users' browsers and giving them the dreaded "browser unresponsive" error. To do this, we take the set of counties to be drawn and divide it into several subsets of counties so the browser can catch its breath between subsets. (For more, see :ref:`nhmc.ctrl.zoomToState`.)

This object sets how many subsets should be used.
``nhmc.config.countySteps.state`` is used for statewide county maps, and
``nhmc.config.countySteps.national`` is used for the nationwide county map.
Their values have been reached through some trial and error in performance
testing, and we don't recommend changing them.


*************
Color options
*************

.. _nhmc.config.styleColors:

nhmc.config.styleColors
=======================

*Object*

This object contains an assortment of named colors for use in various
:ref:`modules <modules>`. It's not used much, but it's there in case it comes
in handy.

.. _nhmc.config.defaultAttributes:

nhmc.config.defaultAttributes
=============================

*Object*

This object contains the default fill and stroke colors for state and county
paths.


***********
City labels
***********

.. _nhmc.config.cityLabels:

nhmc.config.cityLabels
======================

*Object*

This object contains options for the city labels on statewide county maps.

City labels contain three components:

Label
-----

The label is a text element containing the city's name. It has three
configuration options in ``nhmc.config.cityLabels``:

* ``labelNudge`` is used to tweak the alignment of the label and the marker
  (described below). It is represented as a number of pixels the label should
  be moved downward.
* ``labelOpacity`` sets the label's opacity and may be set to any value from 0
  to 1 inclusive; a ``labelOpacity`` of 1 makes all labels completely opaque.
* ``labelSize`` sets the size of the label text in pixels.

Marker
------

The marker is a black circle placed at the city's location on the map. It has
two configuration options in ``nhmc.config.cityLabels``:

* ``markerOpacity`` sets the marker's opacity and may be set to any value from
  0 to 1 inclusive; a ``markerOpacity`` of 1 makes all markers completely
  opaque.
* ``markerRadius`` sets the radius of the marker in pixels.

Background
----------

The background is an optional white box that sits behind the label and marker
to better distinguish them from the map behind them. This isn't currently used
anywhere in the Map Center, but it has three configuration options in
``nhmc.config.cityLabels``:

* ``backgroundOpacity`` sets the background's opacity and may be set to any
  value from 0 to 1 inclusive; a ``backgroundOpacity`` of 1 makes all city
  label backgrounds completely opaque.
* ``backgroundPadding`` sets the amount of extra space on each side of the
  label/marker combination that should be backed by the background element.
  This property is an array in standard CSS order; that is, the four values in
  this array are used for the top, right, bottom and left of the city label, in
  that order.
  
  City labels typically are drawn such that the label is placed to the right of
  the marker. When this is impractical (either because such a placement would
  leave the label cut off by the right edge of the map surface or because this
  label placement would overlap with the label text of another city), though,
  the label is placed to the left of the marker instead. Because of this,
  "right" and left" have special meanings in the ``backgroundPadding``
  property: "right" always refers to the end of the background farthest from
  the marker, and "left" always refers to the end of the background closest
  to the marker.
* ``hideBackgrounds`` should be set to ``true`` if backgrounds should not be
  drawn or ``false`` if they should be drawn.


***********
Identifiers
***********

``lib/map_center/usps_fips.js`` contains a few useful sets of identifiers that
get used frequently.

.. _nhmc.config-usps:

U.S. Postal Service state abbreviations
=======================================

Two objects contain mappings to and from USPS state abbreviations:

* ``nhmc.config.stateToUSPS`` has full state names as keys and abbreviations as
  values.
* ``nhmc.config.USPSToState`` has abbreviations as keys and names as values.

Some states did not report Republican presidential primary results by county in
2012; these states have additional abbreviations listed for maps with one area
for the entire state. For example, the single-area map of Maine corresponds to
a state name of ``Maine (statewide)`` and an abbreviation of ``ME_EMPTY``.

.. _nhmc.config-fips:

FIPS 6-4 county codes
=====================

Two objects contain mappings to and from five-digit `FIPS county codes`_:

.. _FIPS county codes: http://www.epa.gov/enviro/html/codes/state.html

* ``nhmc.config.countyToFIPS`` has full state names as keys and objects as
  values. Those objects in turn have full county (or county-equivalent) names
  as keys and FIPS county codes as values. For example:
  
  .. code-block:: javascript
     
     nhmc.config.countyToFIPS["Virginia"]["Arlington County"] === "51013"
  
  To accommodate the single-area statewide maps described
  :ref:`above <nhmc.config-usps>`, there are also sub-objects for each state
  that has such a map, using the pseudo-FIPS code consisting of the state's
  two-digit FIPS code followed by ``000`` for the "county" comprising the
  entire state. For example:
  
  .. code-block:: javascript
     
     nhmc.config.countyToFIPS["Maine (statewide)"]["Maine"] === "23000"

* ``nhmc.config.FIPSToCounty`` has FIPS county codes as keys and arrays as
  values. Each array contains two elements: The first is the full state name,
  and the second is the full county name. For example:
  
  .. code-block:: javascript
     
     nhmc.config.FIPSToCounty["51013"] === ["Virginia", "Arlington County"]
     nhmc.config.FIPSToCounty["23000"] === ["Maine (statewide)", "Maine"]


*************
Miscellaneous
*************

.. _nhmc.config.hashParams:

nhmc.config.hashParams
======================

*Object*

This object controls the format of values used in a page's
`fragment identifier`_ to let users link to specific map views within
particular :ref:`modules <modules>` or other initial states of module code.
(For example, it could be used to share users' predictions of Electoral College
outcomes.) :ref:`nhmc.ctrl.hashParams` handles the setting and getting of these
values, but ``nhmc.config.hashParams`` controls the characters used to format
them within the fragment identifier.

.. _fragment identifier: http://en.wikipedia.org/wiki/Fragment_identifier
